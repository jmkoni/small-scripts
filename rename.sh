cd ~/Dropbox/Camera\ Uploads
for file in Photo*
do
    newfile=(${file// / })
    date=${newfile[2]}
    date=(${date//,/ })
    echo "$date"

    if [ "${newfile[1]}" == "Jan" ]
    then
        month="2012-01"
    fi

    if [ "${newfile[1]}" == "Feb" ]
    then
        month="2012-04"
    fi

    if [ "${newfile[1]}" == "Mar" ]
    then
        month="2012-04"
    fi

    if [ "${newfile[1]}" == "Apr" ]
    then
        month="2012-04"
    fi

    if [ "${newfile[1]}" == "May" ]
    then
        month="2012-05"
    fi

    if [ "${newfile[1]}" == "Jun" ]
    then
        month="2012-06"
    fi

    if [ "${newfile[1]}" == "Jul" ]
    then
        month="2011-07"
    fi

    if [ "${newfile[1]}" == "Aug" ]
    then
        month="2011-08"
    fi

    if [ "${newfile[1]}" == "Sep" ]
    then
        month="2011-09"
    fi

    if [ "${newfile[1]}" == "Oct" ]
    then
        month="2011-10"
    fi

    if [ "${newfile[1]}" == "Nov" ]
    then
        month="2011-11"
    fi

    if [ "${newfile[1]}" == "Dec" ]
    then
        month="2011-12"
    fi

    test=$month-$date-${newfile[5]}
    echo "$test"
    mv "$file" "$test"

done
